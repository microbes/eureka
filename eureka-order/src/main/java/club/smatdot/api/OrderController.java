package club.smatdot.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class OrderController {
	
	@Autowired
	private RestTemplate restTemplate;

	/***
	 * 在Spring Cloud中有两种方式调用rest、fegin(Spring Cloud)
	 * @return
	 */
	@RequestMapping("/getOrder")
	public String getOrder() {
		// order 使用rpc 远程调用技术 调用 会员服务
		String memberUrl = "http://app-member/getMember";
		String result = restTemplate.getForObject(memberUrl, String.class);
		System.out.println("订单服务调用会员服务,result:" + result);
		return result;
	}

}
