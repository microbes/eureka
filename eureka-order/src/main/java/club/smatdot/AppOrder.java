package club.smatdot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
public class AppOrder {

	public static void main(String[] args) {
		SpringApplication.run(AppOrder.class, args);
		
		// 如果使用rest方式以别名方式进行调用,依赖ribbon负载均衡能力,必须加@LoadBalanced注解
	}

	@Bean
	@LoadBalanced
	// @LoadBalanced就能让这个RestTemplate在请求时拥有客户端负载均衡的能力
	// 解决RestTemplate找不到原因,应该把RestTemplate注册到springboot容器中
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
